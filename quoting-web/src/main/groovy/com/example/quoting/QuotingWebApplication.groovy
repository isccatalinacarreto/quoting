package com.example.quoting

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class QuotingWebApplication {

	static void main(String[] args) {
		SpringApplication.run(QuotingWebApplication, args)
	}

}
